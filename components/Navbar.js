import React from 'react'
import { Grid12 } from 'xf-material-components/package/index'
import Link from 'next/link'

import style from './Navbar.module.css'

function Navbar () {
  const listyle = {
    display: 'flex',
    alignItems: 'center'
  }

  const hold = {
    width: '100%',
    padding: '1em',
    background: 'var(--slate)',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }

  const small = {
    color: 'black',
    textDecoration: 'none',
    fontSize: '1.6em'
  }

  const linkStyle = {
    textDecoration: 'none',
    color: 'black',
    marginRight: '1em'
  }

  return (
    <div className={style.hold}>
      <Grid12>
        <div className={style.listyle}>
            <Link href="/"><a style={linkStyle}><span style={small}>XF</span></a></Link>
            <Link href="/"><a style={linkStyle}>Design and Brand</a></Link>
        </div>
        <div className={style.brand}><Link href="/brand"><a style={linkStyle}>Brand</a></Link></div>
        <div className={style.writing}><Link href="/writing"><a style={linkStyle}>Writing</a></Link></div>
        <div className={style.components}><Link href="/components"><a style={linkStyle}>Components</a></Link></div>
        <div className={style.contribute}><Link href="/contribute"><a style={linkStyle}>Contribute</a></Link></div>
      </Grid12>
    </div>
  )
}

export default Navbar