### Winter Project Areas

#### Design and Brand documentation.
Keywords: #Projects #Brand #Components

[Project Overview](https://gitlab.com/xalgorithms-alliance/xf-material-components/-/milestones/1)

Due Date: January 8th, 2021

Goals: Create a working component library that can be used by xrm dev, the new website and other projects. the components should address the pitfalls faced by the last attempt. the end result should be a coherent set of patterns that can be used accross a variety of scenarios. 

#### XRM dev Landing 
Keywords: #UI #Writing

[Project Overview](https://gitlab.com/xalgorithms-alliance/xf-material-components/-/milestones/2)

Due Date: January 15th, 2021

Goals: Design and develop copy for a landing page that utilizes the building blocks presented in the design and brand documentation.

#### New Website
Keywords: #Projects #Brand #UI #Writing

[Project Overview](https://gitlab.com/xalgorithms-alliance/xf-material-components/-/milestones/3)

Due Date: February 1st 2021

Using this process and refining from experience the next task will be finishing the new Xalgorithms Foundation website. This project is already underway, with the [basic design](https://www.figma.com/file/ypnpEAJXjSZLHL49FP8bvp/Re-Xalgorithms?node-id=2257%3A0) structure defined, and the core web architechture developed.

#### XRM dev UI
Keywords: #Projects #UI 

[Project Overview](https://gitlab.com/xalgorithms-alliance/xf-material-components/-/milestones/4)

Due Date: As time comes

execute the [new design](https://www.figma.com/file/v0P92KBcIJAniZMsPl6Y5k/IDE?node-id=1282%3A2389) using the refined components.

### Background

Xalgorithms Foundation is focused on the development of technical architecture in order to produce a reference implementation for the Oughtomation method. In the spring of this year, thought experiments compelled a system redesign that resulted in a greatly simplified architecture. Accompanying these systems changes, the project has received communications from a number of entities expressing interest in collaboration and adoption. This document is intended to provide a roadmap for user experience considerations, and to help coordinate future efforts as the community of contributors scales.
As the Foundation moves forward, it is clear that user considerations need to accompany technical designs at all scales. While the Foundation has separated user interfaces from the core construction in the past, developments over the past months underscore the fact that the creation of a rule maker interface has become a key component in project development.
I propose that there are two motivating factors now guiding project development. 1. Technical 2. User adoption—number one leading into number two. I propose this because:
  
  - Third party interest has consistently indicated that an effective rule maker interface is a motivating factor in adoption. 
  
  - To advance contributor goals (that range from increasing trade compliance to the creation of a primary commodity based currency anchor), adoption of the Oughtomation method is required. In other words, institutions and individuals need to be using the system and for that to happen, we need to be meeting their 

The majority of the work, thus far, has been on the technical side. This is fortuitous. We know the underlying mechanics for how rules are created, discovered and received. The technical work provides an excellent scaffold from which effective, human-facing applications can be built. Rather than create unwieldy constraints, the opposite is true. The technical imperative of tabular programming compels a simple, intuitive method of rule authorship. This was by design. At the macro level a strong union between technical design, and user needs has always existed. However, at the granular level of development, this union has been more difficult to maintain. 
Since July, there has been significant effort put into not only developing technical reference implementations, but also in constructing a reference implementation rule maker interface (XRM). This sustained a period of momentum, before dropping off. My intention is to use the writing to rekindle that trajectory by better linking motivating areas, and by establishing common goals, and checkpoints along the way. 

[//]: # (This is a comment that will not be rendered)

## Challenges & Solutions

Similarly, there are contextual challenges that must be recognized at this point in development. 
Limited time from developers. The biggest challenge facing development across all areas is the limited time availability. This is not inherently a problem. In the first chapter of Ryan Singer Shape Up, he describes having only a few hours of developer time a week in the early stages of Basecamp. This didn’t stall the project, instead it forced strategic decision making to ensure that time was spent wisely.
Limited project management. While the macro level goals of the Xalgorithm Foundation are clear, determining how those translate into the micro scale of daily development tasks is less apparent. My hope is that by articulating project context and by mapping out options, it will be easier to align and execute on the smaller tasks that combine to fulfil macro goals. 
Likely rapid growth in the coming months. With growing interest, it is possible that new contributors will begin participating. This is an exciting development that will enable more rapid development. However it will require us to meet this participation with corresponding organization. 
A wide range of project types. User experience considerations must extend across many public facing areas of the foundation. This means that there are many projects that come under this purvey, from XRM, XRT, the Xalgorithms website, to a proposed interactive ERA research website. All of these are worth contributing to. Creating discrete project areas and options will help to ensure better triage and more efficient progress.   
Poorly understood user needs. At the moment the general purpose requirements of XRM means that learning from our own experience making rules using the interface has been enough to get started. However, as collaboration begins, there will likely be research required in to build effective XRT reference implementations. This should not be our prerogative to provide. However it will be crucial to voice such needs as they arise. 

