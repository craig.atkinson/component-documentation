[//]: # (you can learn about markdown syntax here https://www.markdownguide.org/basic-syntax)

[//]: # (You can write comments like this, and they will be visible to editors, but will not be rendered on the page)

### Writing Comes from the Community

As an open source project, not only does code and documentation come from the community, so does our communications strategy, and web content. This document is intended to provide rough guidlines for those interested in contributing writing.

### The Philosophy Informing Writing

The philosophy behind the Xalgorithms Foundation's communications strategy is that Oughtomation's benefits are best expressed by highlighting the multiple voices and perspectives involved in Xalgorithms Foundation collaborations. 




### Terms

Distinguishing oughtomation from artificial intelligence (AI) - Oftentimes rule makers and rules takers conflate algorithmic policy with the automation of legal enforcement. This may then lead to the further association of artificial intelligence for automated compliance (in other words, ‘autocratic’)  systems  whether hosted in the physical or digital realm. Our human-centred approach emphasizes the need for human-readable expression, expectation-setting, and the anticipation of error in ‘oughtomation’. Our approach is rooted in maintaining end-user agency and artificial naïvety, rather than rule-maker autocracy and artificial intelligence.

Emphasizing the importance of tolerance in system design - The designer of a system is responsible for considering the potential interface, experience, and prerogative of its users in their totality. Without considering the user’s own experience and own prerogatives, the designer of a system may be responsible for a convoluted interpretation of the system and may not place adequate significance upon an end-user's autonomous perception of how it is or is not useful to them. This may interfere with practical experimentation, implementation and adoption.

Highlighting the benefits of interoperability -To truly enable end-user agency in ‘oughtomation’ it is critical that individuals in the public and private sector are empowered to develop solutions/applications that carry out the method without stringent restrictions in the oughtomation specification as to how. Private and public sector initiatives often create silos that lead to competition rather than cooperation. ‘Co-opetition’ through interoperability enables greater market access and user-choice in an evermore interconnected world. It is essential for equitable interaction between rule makers and rule takers in digital systems. With the rise in development and adoption of a variety of distributed systems protocols for myriad use-cases, it is vital that ‘oughtomation’ comes to be understood as not a replacement for, but an auxiliary capability to enhance any other systems, networks, and applications. 

Market demand and social preference for transparency and simplicity - Technological innovation often leaves its adopters in the dark about the inner workings, restrictions and/or issues that they entail for society. Principles inherent in free/libre/open licensing and methods have long been driven by market demand and social preference for greater transparency and simplicity in critical system design. Demand for greater transparency will bring competition among designers and compel them to embody simplicity as a core principle to ensure end-user agency. ‘Oughtomation’ as a method embodies simplicity as a core function of design, and thus must also demonstrate such in its communication to its end-users and implementers. 



