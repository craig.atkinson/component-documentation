import { Button, CodeArea, InputDate, InputText, TextArea, InputCheck, InputSlider, InputDropdown, Icon, Column, Grid12, LinkButton } from 'xf-material-components/package/index'
import Container from '../components/Container'
import Header from '../components/Header'


const Components = () => {
  const gret = {
    color: 'var(--steel)',
    fontWeight: '400'
  }

  const imgHold = {
    width: '80%',
    margin: 'auto',
    maxWidth: '900px'
  }
  const cover = {
    width: '100%'
  }

  const links = {
    position: '-webkit-sticky',
    position: 'sticky',
    top: '0',
    height: '200px',
    gridArea: '1/2/1/4'
  }

  const content = {
    gridArea: '1/4/1/8',
    width: '550px'
  }

  const padding = {
    paddingTop: '2em'
  }

  // eslint-disable-next-line quotes
  const install = `$ yarn add xf-material-components`

  const reactstyle = `// App.js

import { Button, InputText, InputDate } from 'xf-material-components'
import GlobalStyle from 'xf-material-components/config/global.styles'
  
function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <header className="App-header">
        <Button>Hello World</Button>
        <InputText />
        <InputDate />
      </header>
    </div>
  );
}`

  const nextjs = `// index.js

  import 'xf-material-components/config/rootStyle.css'`

  const iconBlock = `<Icon name="callendar"/>
<Icon name="ArrowRight" />
<Icon name="ArrowLeft" />
<Icon name="Upload" />
<Icon name="Download" />
<Icon name="Reset" />
<Icon name="Trash" />
<Icon name="External" />
<Icon name="Add" />
<Icon name="Toggle" />
<Icon name="Info" />
<Icon name="Settings" />
<Icon name="Standards" />
<Icon name="Edit" />`

  const textinputBlock = `import React from 'react'
import { InputText } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputText 
      value={yourString} 
      placeHolder={yourString} 
      onChange={yourFunction} 
      errorMessage={yourString}
    />
  )
}

export default YourFunction`

  const textareaBlock = `import React from 'react'
import { TextArea } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputText 
      value={yourString} 
      placeHolder={yourString} 
      onChange={yourFunction} 
      errorMessage={yourString}
    />
  )
}

export default YourFunction`

  const checkBlock = `// need to add logic
import React from 'react'
import { InputCheck } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputCheck />
  )
}

export default YourFunction`

  const sliderBlock = `import React from 'react'
import { InputSlider } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputSlider onChange={yourFunction} />
  )
}

export default YourFunction`

  const dropdownBlock = `import React from 'react'
import { InputDropdown } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputDropdown
      onChange={yourFunction}
      options={[
        { value: 'yourValue', label: 'yourLabel' }
      ]} 
     />
  )
}

export default YourFunction`

  const dateBlock = `import React from 'react'
import { InputDate } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputDate 
      value={yourString} 
      placeHolder={yourString} 
      onChange={yourFunction} 
    />
  )
}

export default YourFunction`

  const type = `// paragraph
  
<p>Text<p>
<p className="baskerville">Text<p>

// inline elements

<a>Link</a>
<i>Italic</i>
<p className="baskerville"><i>Italic</i></p>
<code>inline code</code>
<mark>highlight</mark>`

  const h = `<h1>Rules as data</h1>
<h2>Rules as data</h2>
<h3>Rules as data</h3>
<h4>Rules as data</h4>
<h5>Rules as data</h5>`

  const block = `import React from 'react'
import { CodeArea } from 'xf-material-components'

const YourFunction = () => {
  const yourCode = ˴this is how you use a code block˴
  return (
    <CodeArea code={yourCode}/>
  )
}

export default YourFunction`

  const buttonBlock = `import React from 'react'
  import { Button } from 'xf-material-components'

  const YourFunction = () => {
    return (
      <Button>Hello World </Button>
      <Button type="clear">Hello World</Button>
      <Button type="disabled">Hello World</Button>
      <Button type="wide">Hello World</Button>  
    )
  }
  
  export default YourFunction`

  // eslint-disable-next-line quotes
  const serif = `<h2 className="baskervilleHeadline">Your text</h2>`
  return (
    <>
    <Header 
      title="The Component Language."
      description="Simple, modular React components that build Xalgorithms Foundation interfaces."
      buttonLable="See the Code"
      target="https://gitlab.com/xalgorithms-alliance/xf-material-components"
    />
    <Grid12>
      <div style={links}>
        <Column>
          <div style={padding}/>
          <div>
            <LinkButton label="Installation" target="#iconic"/>
          </div>
          <div>
            <LinkButton label="Icons" target="#iconic"/>
          </div>
          <div>
            <LinkButton label="Type" target="#iconic"/>
          </div>
          <div>
            <LinkButton label="Buttons" target="#iconic"/>
          </div>
          <div>
            <LinkButton label="Inputs" target="#iconic"/>
          </div>
          <div>
            <LinkButton label="Layout" target="#iconic"/>
          </div>
        </Column>
      </div>
      <div style={content}>
        <Container>
          <div className="head">
            <Column>
              <h3>Installation</h3>
              <p>For installation, download the package into your project folder.</p>
              <CodeArea code={install}/>
              <p>For React projects import <code>GlobalStyle</code> into the root of your app and add the <code>GlobalStyle</code> component into your code. </p>
              <CodeArea code={reactstyle}/>
              <p>For next.js projects, import the <code>rootStyle</code> into your index.js file.</p>
              <CodeArea code={nextjs}/>
            </Column>
          </div>
          <div className="head" id="iconic">
            <Column>
              <h3>Icons</h3>
            </Column>
          </div>
          <div className="head">
            <Column>
              <div className="simpleFlex">
              <Icon name="callendar"/>
              <Icon name="ArrowRight" />
              <Icon name="ArrowLeft" />
              <Icon name="Upload" />
              <Icon name="Download" />
              <Icon name="Reset" />
              <Icon name="Trash" />
              <Icon name="External" />
              <Icon name="Add" />
              <Icon name="Toggle" />
              <Icon name="Info" />
              <Icon name="Settings" />
              <Icon name="Standards" />
              <Icon name="Edit" />
              </div>
              <CodeArea code={ iconBlock }/>
            </Column>
          </div>
          <div className="head">
            <Column>
              <h3>Type</h3>
            </Column>
          </div>
          <div className="head">
            <Column>
              <h4 style={gret}>
                Titles
              </h4>
              <div className="fontFlex">
                <p className="formSmall">h1</p>
                <h1>Rules as data</h1>
              </div>
              <div className="fontFlex">
                <p className="formSmall">h2</p>
                <h2>Rules as data</h2>
              </div>
              <div className="fontFlex">
                <p className="formSmall">h3</p>
                <h3>Rules as data</h3>
              </div>
              <div className="fontFlex">
                <p className="formSmall">h4</p>
                <h4>Rules as data</h4>
              </div>
              <div className="fontFlex">
                <p className="formSmall">h5</p>
                <h5>Rules as data</h5>
              </div>
              <CodeArea code={ h }/>
            </Column>
          </div>
          <div className="head">
            <Column>
              <h4 style={gret}>
                Serif Contrast
              </h4>
              <h2 className="baskervilleHeadline">Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit</h2>
              <CodeArea code={ serif }/>
            </Column>
          </div>
          <div className="head">
            <Column>
            <h4 style={gret}>
              Paragraph
            </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ridiculus tempor, dignissim lectus justo, etiam condimentum. </p>
            <p className="baskerville">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ridiculus tempor, dignissim lectus justo, etiam condimentum. </p>
            <div className="simpleFlex">
              <a>Link</a>
              <p><i>Italic</i></p>
              <p className="baskerville"><i>Italic</i></p>
              <code>inline code</code>
              <mark>highlight</mark>
            </div>
            <CodeArea code={ type }/>
            </Column>
          </div>
          <div className="head">
            <Column>
              <h4 style={gret}>
                Code Block
              </h4>
              <CodeArea code={ block }/>
            </Column>
          </div>
          <div className="head">
            <Column>
              <h3>Buttons</h3>
            </Column>
          </div>
          <div className="head">
            <Column>
            <h4 style={gret}>
              Buttons
            </h4>
            <div className="simpleFlex">
              <Button>Hello World </Button>
              <Button type="clear">Hello World</Button>
              <Button type="disabled">Hello World</Button>
            </div>
            <Button type="wide">Hello World</Button>
            <CodeArea code={ buttonBlock }/>
            </Column>
          </div>
          {/*
          <div className="head">
            <h4 style={gret}>
              Buttons
            </h4>
            <div className="simpleFlex">
              <Tabs label="test" active="true" />
            </div>
            <CodeArea code={ buttonBlock }/>
          </div>
      */}
        <div className="head">
          <Column>
            <h3>Inputs</h3>
          </Column>
        </div>
        <div className="head">
          <Column>
            <h4 style={gret}>
                Input Date
            </h4>
          </Column>
        </div>
        <div className="head">
          <Column>
            <InputDate />
            <CodeArea code={ dateBlock }/>
          </Column>
        </div>
        <div className="head">
          <Column>
            <h4 style={gret}>
                Input Text
            </h4>
            <InputText />
            <InputText errorMessage="An error"/>
            <CodeArea code={ textinputBlock }/>
          </Column>
        </div>
        <div className="head">
          <Column>
            <h4 style={gret}>
                Text Area
            </h4>
            <TextArea />
            <TextArea errorMessage="An Error"/>
            <CodeArea code={ textareaBlock }/>
          </Column>
        </div>
        <div className="head">
          <Column>
            <h4 style={gret}>
                Check
            </h4>
            <InputCheck />
            <CodeArea code={ checkBlock }/>
          </Column>
        </div>
        <div className="head">
          <Column>
            <h4 style={gret}>
                Slider
            </h4>
            <InputSlider />
            <CodeArea code={ sliderBlock }/>
          </Column>
        </div>
        <div className="head">
          <Column>
            <h4 style={gret}>
                Dropdown
            </h4>
            <InputDropdown
            options={[
              { value: 'test', label: 'Test' },
              { value: 'test', label: 'Test' }
            ]}/>
            <CodeArea code={ dropdownBlock }/>
          </Column>
        </div>
      </Container>
    </div>
  </Grid12>
  </>
  )
}

export default Components
