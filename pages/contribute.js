import { getPostData } from '../lib/posts'
import { Column, Grid12 } from 'xf-material-components/package/index'
import Header from '../components/Header'

export async function getStaticProps() {
    const postData = await getPostData('contribution-guidelines')
    return {
      props: {
        postData
      }
    }
  }

  const content = {
    gridArea: '1/4/1/8',
    width: '550px'
  }

export default function contribute({postData}) {
    const gret = {
        color: 'var(--steel)',
        fontWeight: '400'
      }
    return (
      <div>
        <Header 
          title="Building in public. By the public."
          description="See what&apos;s in the pipeline, and where you can lend a hand."
          buttonLable="Project Overview"
          target="https://gitlab.com/xalgorithms-alliance/xf-material-components/-/milestones/1"
        />
        <Grid12>
          <div style={content}>
            <Column>
              <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="SetWritingHold"/>
            </Column>
          </div>
        </Grid12>
      </div>
    )
  }