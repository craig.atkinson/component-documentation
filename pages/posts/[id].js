import Head from 'next/head'
import { getAllPostIds, getPostData } from '../../lib/posts'


export async function getStaticPaths() {
    const paths = getAllPostIds()
    return {
      paths,
      fallback: false
    }
  }

  export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id)
    return {
      props: {
        postData
      }
    }
  }


  

  export default function Post({ postData }) {
    return (
      <div>
        <Head>
          <title>{postData.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main>
            <h2>{postData.title}</h2>
            <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="textm"/>
        </main>
      </div>
    )
  }

  