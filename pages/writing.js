import { getPostData } from '../lib/posts'
import { Column, Grid12 } from 'xf-material-components/package/index'
import Header from '../components/Header'

export async function getStaticProps() {
    const postData = await getPostData('writing-guidelines')
    return {
      props: {
        postData
      }
    }
  }

  const content = {
    gridArea: '1/4/1/8',
    width: '550px'
  }

export default function writing({postData}) {
    return (
      <div>
        <Header 
          title="Writing"
          description="Lorem Ipsum"
          buttonLable="CTA"
          target="https://gitlab.com/xalgorithms-alliance/xf-material-components"
        />
        <Grid12>
          <div style={content}>
            <Column>
              <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="SetWritingHold"/>
            </Column>
          </div>
        </Grid12>
      </div>
    )
  }